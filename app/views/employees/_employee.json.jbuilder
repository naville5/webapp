json.extract! employee, :id, :employee_index, :employee_name, :age, :designation, :created_at, :updated_at
json.url employee_url(employee, format: :json)
