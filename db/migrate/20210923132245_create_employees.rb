class CreateEmployees < ActiveRecord::Migration[6.1]
  def change
    create_table :employees do |t|
      t.string :employee_index
      t.string :employee_name
      t.string :age
      t.string :designation

      t.timestamps
    end
  end
end
